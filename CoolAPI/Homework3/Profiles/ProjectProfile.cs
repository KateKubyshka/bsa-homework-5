﻿using AutoMapper;
using DAL.Entities;
using WebAPI.DTOs;

namespace WebAPI.Profiles
{
    public class ProjectProfile : Profile
    {
        public ProjectProfile()
        {
            CreateMap<Project, ProjectDTO>();
            CreateMap<ProjectDTO, Project>();
        }
    }
}
