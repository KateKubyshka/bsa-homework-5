﻿using AutoMapper;
using DAL.Entities;
using WebAPI.DTOs;

namespace WebAPI.Profiles
{
    public class TeamProfile : Profile
    {
        public TeamProfile()
        {
            CreateMap<Team, TeamDTO>();
            CreateMap<TeamDTO, Team>();
        }
    }
}
