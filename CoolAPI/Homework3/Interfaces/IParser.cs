﻿using System.Collections.Generic;

namespace WebAPI.Interfaces
{
    interface IParser
    {
        IEnumerable<T> Parse<T>(string path);
    }
}
