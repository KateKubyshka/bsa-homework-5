﻿using System;
using System.Collections.Generic;

namespace DAL.Entities
{
    public class User : Entity
    {
        public int? TeamId { get; set; }
        public Team Team { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public ICollection<Project> Projects { get; set; }
        public ICollection<ProjectTask> Tasks { get; set; }
        public string Email { get; set; }
        public DateTime RegisteredAt { get; set; }
        public DateTime BirthDay { get; set; }

        public User()
        {
            Projects = new List<Project>();
            Tasks = new List<ProjectTask>();
        }
    }
}
