﻿namespace LinqPracticeLesson1
{
    public class UserClass
    {
        public User User { get; set; }
        public Project LastProject { get; set; }
        public int? CountOfTasks { get; set; }
        public int? AmountOfUnfinishedTasks { get; set; }
        public ProjectTask TheLongestTask { get; set; }
    }
}
