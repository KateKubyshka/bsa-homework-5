﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LinqPracticeLesson1
{
    public class Connector
    {
        public IEnumerable<Project> Data { get; set; }

        public async Task BuildHierarhyAsync()
        {
            IEnumerable<Project> projectsCollection = await HttpManager.GetProjects();
            IEnumerable<ProjectTask> tasksCollection = await HttpManager.GetProjectTasks();
            IEnumerable<Team> teamsCollection = await HttpManager.GetTeams();
            IEnumerable<User> usersCollection = await HttpManager.GetUsers();

            Data = projectsCollection.GroupJoin(
                tasksCollection,
                p => p.Id,
                t => t.ProjectId,
                (proj, tasks) =>
                {
                    proj.Tasks = tasks.Join(
                        usersCollection,
                        t => t.PerformerId,
                        u => u.Id,
                        (task, performer) =>
                        {
                            task.Performer = performer;
                            return task;
                        });

                    return proj;
                })
                .Join(
                usersCollection,
                p => p.AuthorId,
                u => u.Id,
                (proj, user) =>
                {
                    proj.Author = user;
                    return proj;
                })
                .Join(
                teamsCollection,
                p => p.TeamId,
                t => t.Id,
                (proj, team) =>
                {
                    proj.Team = team;
                    return proj;
                });
        }

        public Dictionary<Project, int> GetTasksByUserId(int id)
        {
            return Data.Where(p => p.AuthorId == id)
                       .Select(p => new { Project = p, Count = p.Tasks.Count() })
                       .ToDictionary(k => k.Project, v => v.Count);
        }

        public List<ProjectTask> GetUserTasks(int id)
        {
            return Data.SelectMany(p => p.Tasks)
                       .Where(t => t.PerformerId == id && t.Name.Length < 45)
                       .ToList();
        }

        public List<(int Id, string Name)> GetTaskFinishedIn2021(int id)
        {
            return Data.SelectMany(p => p.Tasks)
                       .Where(t => t.PerformerId == id && t.FinishedAt?.Year == 2021)
                       .Select(t => (t.Id, t.Name))
                       .ToList();
        }

        public List<(int Id, string Name, List<User> Users)> GetTeamsByUsersAge()
        {
            int yearDifference = 10;
            return Data.Select(p => p.Tasks.Select(t => t.Performer)
                        .Append(p.Author))
                        .SelectMany(u => u)
                        .Distinct()
                        .Join(Data.Select(t => t.Team), u => u.TeamId, team => team.Id, (user, team) => new { User = user, Team = team })
                        .OrderByDescending(t => t.User.RegisteredAt)
                        .GroupBy(p => p.Team)
                        .Select(u => (u.Key.Id, u.Key.Name, Users: u.Select(u => u.User).ToList()))
                        .Where(x => x.Users.All(u => DateTime.Now.Year -  u.BirthDay.Year > yearDifference))
                        .ToList();
        }


        public List<(User User, List<ProjectTask> Tasks)> GetUserTasks()
        {
            return Data.Select(p => p.Tasks.Select(t => t.Performer)
                                            .Append(p.Author))
                                            .SelectMany(u => u)
                                            .Distinct()
                                            .OrderBy(p => p.FirstName)
                        .GroupJoin(Data.SelectMany(p => p.Tasks),
                                p => p.Id,
                                t => t.PerformerId,
                                (user, tasks) => ( user, tasks.OrderByDescending(t => t.Name.Length)
                                                              .ToList()))
                                                              .ToList();
        }

        public UserClass GetUserInfo(int id)
        {
          return Data.Where(p => p.AuthorId == id)
                 .OrderByDescending(p => p.CreatedAt)
                 .Select(p => new UserClass
                 {
                     LastProject = p,
                     CountOfTasks = p.Tasks?.Count(),
                     User = p.Author,
                     AmountOfUnfinishedTasks = Data.SelectMany(p => p.Tasks)
                                                .Where(t => t.PerformerId == id && t.FinishedAt is null)
                                                .Count(),
                     TheLongestTask = Data.SelectMany(p => p.Tasks)
                                                .Where(t => t.PerformerId == id && t.FinishedAt is not null)
                                                .Select(t => new { Task = t, Duration = t.FinishedAt - t.CreatedAt })
                                                .OrderByDescending(t => t.Duration)
                                                .First().Task
                 })
                 .FirstOrDefault();
        }

        public IEnumerable<ProjectClass> GetProjects()
        {
            return Data.Select(p => new
            {
                        Project = p,
                        LongestTask = p.Tasks?.OrderByDescending(t => t.Description.Length).FirstOrDefault(),
                        ShortestTask = p.Tasks?.OrderBy(t => t.Name).FirstOrDefault()})
                    .GroupJoin(Data.Select(p => p.Tasks.Select(t => t.Performer).Append(p.Author))
                                .SelectMany(u => u)
                                .Distinct(),
                                p => p.Project.TeamId,
                                u => u.TeamId,
                                (proj, users) => new ProjectClass
                                {
                                    Project = proj.Project,
                                    LongestTask = proj.LongestTask,
                                    ShortestTask = proj.ShortestTask,
                                    AmountOfMembers = users.Count()
                                })
                    .Where(p => p.Project.Description.Length > 20 || p.Project.Tasks.Count() < 3);
        }
    }
}
