﻿using AutoMapper;
using DAL;
using DAL.Entities;
using DAL.Interfaces;
using DAL.Repositories;
using Microsoft.EntityFrameworkCore;
using WebAPI.Controllers;
using WebAPI.DTOs;
using WebAPI.Profiles;
using Xunit;

namespace CoolAPI.Tests
{
    public class UserControllerTests 
    {
        private readonly ProjectDbContext _context;
        private readonly IGenericRepository<User> _projectsRepository;
        private readonly IMapper _mapper;
        public UserControllerTests()
        {
            var mapperConfiguration = new MapperConfiguration(configuration =>
            {
                configuration.AddProfile<ProjectProfile>();
                configuration.AddProfile<ProjectTaskProfile>();
                configuration.AddProfile<TeamProfile>();
                configuration.AddProfile<UserProfile>();
            });
            _mapper = mapperConfiguration.CreateMapper();

            var dbOptionsBuilder = new DbContextOptionsBuilder<ProjectDbContext>().UseInMemoryDatabase("Users");

            _context = new ProjectDbContext(dbOptionsBuilder.Options);

            _projectsRepository = new EFRepository<User>(_context);
        }

        [Fact]
        public void PutOneUserToDb_ThenOneUserExists()
        {
            var controller = new UsersController(_projectsRepository, _mapper);
            var user = new UserDTO()
            {
                Id = 1,
                FirstName = "Alice",
                TeamId = 1,
            };
            controller.Put(user);
            Assert.NotNull(_context.Users.Find(user.Id));
            Assert.Equal("Alice", _context.Users.Find(user.Id).FirstName);
            Assert.Equal(1, _context.Users.Find(user.Id).TeamId);
        }
    }
}
