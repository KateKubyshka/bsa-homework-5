using AutoMapper;
using DAL;
using DAL.Entities;
using DAL.Interfaces;
using DAL.Repositories;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using WebAPI.Controllers;
using WebAPI.DTOs;
using WebAPI.Profiles;
using Xunit;

namespace CoolAPI.Tests
{
    public class ProjectsControllerTests
    {
        private readonly ProjectDbContext _context;
        private readonly IGenericRepository<Project> _projectsRepository;
        private readonly IMapper _mapper;

        public ProjectsControllerTests()
        {
            var mapperConfiguration = new MapperConfiguration(configuration =>
            {
                configuration.AddProfile<ProjectProfile>();
                configuration.AddProfile<ProjectTaskProfile>();
                configuration.AddProfile<TeamProfile>();
                configuration.AddProfile<UserProfile>();
            });
            _mapper = mapperConfiguration.CreateMapper();

            var dbOptionsBuilder = new DbContextOptionsBuilder<ProjectDbContext>().UseInMemoryDatabase("Projects");

            _context = new ProjectDbContext(dbOptionsBuilder.Options);

            _projectsRepository = new EFRepository<Project>(_context);
        }

        [Fact]
        public void Put_WhenPutOneProject_ThenOneProjectInDb()
        {
            var controller = new ProjectsController(_projectsRepository, _mapper);
            var model = new ProjectDTO
            {
                Id = 1,
                AuthorId = 1,
                Deadline = DateTime.Now,
                Description = "Description",
                Name = "Name",
                TeamId = 1
            };

            controller.Put(model);

            Assert.NotNull(_context.Projects.Find(model.Id));
        }
    }
}
