﻿using AutoMapper;
using DAL;
using DAL.Entities;
using DAL.Interfaces;
using DAL.Repositories;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebAPI.Controllers;
using WebAPI.DTOs;
using WebAPI.Profiles;
using Xunit;

namespace CoolAPI.Tests
{
    public class ProjectTaskControllerTests
    {
        private readonly ProjectDbContext _context;
        private readonly IGenericRepository<ProjectTask> _projectsRepository;
        private readonly IMapper _mapper;
        public ProjectTaskControllerTests()
        {
            var mapperConfiguration = new MapperConfiguration(configuration =>
            {
                configuration.AddProfile<ProjectProfile>();
                configuration.AddProfile<ProjectTaskProfile>();
                configuration.AddProfile<TeamProfile>();
                configuration.AddProfile<UserProfile>();
            });
            _mapper = mapperConfiguration.CreateMapper();

            var dbOptionsBuilder = new DbContextOptionsBuilder<ProjectDbContext>().UseInMemoryDatabase("Tasks");

            _context = new ProjectDbContext(dbOptionsBuilder.Options);

            _projectsRepository = new EFRepository<ProjectTask>(_context);
        }
        [Fact]
        public void SetProjectTaskFinished_ThenTaskFinishedAtNotNull()
        {
            var controller = new TasksController(_projectsRepository, _mapper);
            var task1 = new ProjectTask() { Id = 1, ProjectId = 1, FinishedAt = null };
            var project = new Project()
            {
                Id = 1,
                Name = "First",
                TeamId = 1,
                Tasks = { task1 }
            };
            _context.Add(project);
            _context.SaveChanges();
            var model = new ProjectTaskDTO()
            {
                Id = 1,
                ProjectId = 1,
                FinishedAt = new DateTime(2021, 2, 2)
            };
            controller.Post(model);
            Assert.NotNull(_context.Projects.Find(model.ProjectId).Tasks.First().FinishedAt);
        }
    }
    
}
