using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace LinqPracticeLesson1.Tests
{

    public class ConnectorTests
    {

        [Fact]
        public void GetTasksByUserId_WhenUserHas2ProjectsWithTasks_ThenGet2ProjectWithTasks()
        {
            Connector connector = InitializeData(out Project proj1, out Project proj2, out _, out _);
            var result = connector.GetTasksByUserId(1);

            Assert.Equal(2, result.Count);
            Assert.Equal(10, result[proj1]);
            Assert.Equal(4, result[proj2]);
        }

        private static Connector InitializeData(out Project proj1, out Project proj2, out Project proj3, out Project proj4)
        {
            proj1 = new Project
            {
                AuthorId = 1,
                TeamId = 1,
                Tasks = Enumerable.Range(1, 10).Select(id => new ProjectTask { Id = id })
            };
            proj2 = new Project
            {
                AuthorId = 1,
                TeamId = 1,
                Tasks = Enumerable.Range(20, 4).Select(id => new ProjectTask { Id = id })
            };
            proj3 = new Project
            {
                AuthorId = 2,
                TeamId = 2,
                Tasks = Enumerable.Range(40, 40).Select(id => new ProjectTask { Id = id })
            };
            proj4 = new Project
            {
                AuthorId = 2,
                TeamId = 3,
                Tasks = Enumerable.Range(40, 40).Select(id => new ProjectTask { Id = id })
            };
            return new Connector
            {
                Data = new List<Project> { proj1, proj2, proj3, proj4 }
            };
        }

        [Fact]
        public void GetTasksByUserd_WhenUserHasNoProject_ThenResultEmpty()
        {
            Connector connector = InitializeDataWithEmptyTasks(out _, out _, out Project proj3);

            var actual = connector.GetTasksByUserId(3);
            Assert.Empty(actual);

        }
        [Fact]
        public void GetTaskByUserId_WhenUserHasNotProject_ThenDictionaryDoesNotContainIt()
        {
            Connector connector = InitializeDataWithEmptyTasks(out _, out _, out Project proj3);
            var actual2 = connector.GetTasksByUserId(1);
            Assert.DoesNotContain(proj3, actual2.Keys);
        }

        private static Connector InitializeDataWithEmptyTasks(out Project proj1, out Project proj2, out Project proj3)
        {
            proj1 = new Project
            {
                Id = 1,
                Name = "First",
                Tasks = Enumerable.Empty<ProjectTask>(),
                AuthorId = 1
            };
            proj2 = new Project
            {
                Id = 2,
                Name = "Second",
                Tasks = Enumerable.Empty<ProjectTask>(),
                AuthorId = 1
            };
            proj3 = new Project
            {
                Id = 2,
                Name = "Third",
                Tasks = Enumerable.Empty<ProjectTask>(),
                AuthorId = 2
            };
            return new Connector
            {
                Data = new List<Project> { proj1, proj2, proj3 }
            };
        }

        [Fact]
        public void GetUserTasks_WhenItHas2TasksWithNameLengthLessThan45Symb_ThenResultIs2()
        {
            Connector connector = InitializeData(out Project proj1, out Project proj2, out _, out _);

            AddTasksToProjects(proj1, out ProjectTask task1, out ProjectTask task2, out ProjectTask task3, out ProjectTask task4);
            List<ProjectTask> actual = connector.GetUserTasks(2);
            Assert.Equal(2, actual.Count);
            Assert.DoesNotContain(task1, actual);
            Assert.DoesNotContain(task2, actual);
            Assert.Contains(task3, actual);
            Assert.Contains(task4, actual);
        }

        private static void AddTasksToProjects(Project proj1, out ProjectTask task1, out ProjectTask task2, out ProjectTask task3, out ProjectTask task4)
        {
            task1 = new()
            {
                Id = 1,
                PerformerId = 2,
                Name = "VeryStrangwTaskWithVeryLongNameButWithoutDrawbacksAndItsFine",
                FinishedAt = null,
            };
            task2 = new()
            {
                Id = 1,
                PerformerId = 2,
                Name = "VeryStrangwTaskWithVeryLongNameButWithoutDrawbacksAndItsFineAndAlsoGoodHumour",
                FinishedAt = new System.DateTime(2020, 5, 22),
            };
            task3 = new()
            {
                Id = 1,
                PerformerId = 2,
                Name = "VeryShortTask",
                FinishedAt = new System.DateTime(2021, 5, 21),
            };
            task4 = new()
            {
                Id = 1,
                PerformerId = 2,
                Name = "VeryShortTask2",
                FinishedAt = new System.DateTime(2021, 5, 20),
            };
            proj1.Tasks = new List<ProjectTask>
            {
                task1, task2, task3, task4
            };
        }

        [Fact]
        public void GetTaskFinishedIn2021_WhenUserHas2FinishedTasks_ThenTasksCountEquel2()
        {
            Connector connector = InitializeData(out Project proj1, out _, out _, out _);
            AddTasksToProjects(proj1, out ProjectTask task1, out ProjectTask task2, out ProjectTask task3, out ProjectTask task4);
            ProjectTask task5 = new()
            {
                Id = 1,
                PerformerId = 2,
                Name = "VeryShortTask2",
                FinishedAt = new System.DateTime(2020, 5, 20),
            };
            proj1.Tasks = proj1.Tasks.Append(task5);
            var actual = connector.GetTaskFinishedIn2021(2);
            Assert.Equal(2, actual.Count);
        }

        [Fact]
        public void GetTaskFinishedIn2021_WhenTasksAreEmpty_ThenListEmpty()
        {
            Connector connector = InitializeDataWithEmptyTasks(out Project proj1, out Project proj2, out Project proj3);
            var actual = connector.GetTaskFinishedIn2021(1);
            Assert.Empty(actual);
        }

        [Fact]
        public void GetTeamsByUsersAge_WhenSingleUserOlderThan10_ThenSingleUserInList()
        {
            User user1 = new()
            {
                Id = 1,
                FirstName = "Luka",
                BirthDay = new System.DateTime(2000, 4, 22),
                TeamId = 1
            };
            User user2 = new()
            {
                Id = 2,
                FirstName = "Andrey",
                BirthDay = new System.DateTime(2015, 4, 22),
                TeamId = 2
            };
            ProjectTask task1 = new()
            {
                Id = 1,
                ProjectId = 1,
                PerformerId = 1,
                Performer = user1,
            };
            ProjectTask task2 = new ProjectTask()
            {
                Id = 2,
                ProjectId = 2,
                PerformerId = 2,
                Performer = user2
            };
            Team team1 = new()
            {
                Id = 1,
                Name = "First"
            };
            Team team2 = new()
            {
                Id = 2,
                Name = "Second"
            };
            Project Proj1 = new()
            {
                Id = 1,
                AuthorId = 1,
                Tasks = new List<ProjectTask>()
                {
                    task1
                },
                Author = user1,
                TeamId = 1,
                Team = team1,
            };
            Project Proj2 = new()
            {
                Id = 2,
                AuthorId = 2,
                Author = user2,
                Tasks = new List<ProjectTask>()
                {
                    task2
                },
                TeamId = 2,
                Team = team2,
            };
            Connector connector = new();
            connector.Data = new List<Project>() { Proj1, Proj2 };
            var actual = connector.GetTeamsByUsersAge();
            Assert.Single(actual.First().Users);
        }

        [Fact]
        public void GetAllUserTasksOrderedBy_When3Users_ThenResultSortedUsersByNameAndSortedTasks()
        {
            Connector connector = new();
            User user1 = new()
            {
                Id = 1,
                FirstName = "Luccia",

            };
            User user2 = new()
            {
                Id = 2,
                FirstName = "Alice"
            };
            User user3 = new()
            {
                Id = 3,
                FirstName = "Vasya"
            };
            Project proj1 = new()
            {
                Id = 1,
                AuthorId = 1,
                Author = user1,
                Tasks = Enumerable.Range(1, 3).Select(id => new ProjectTask() { Id = id, Name = "Task" + id, PerformerId = 1, Performer = user1 })

            };
            Project proj2 = new()
            {
                Id = 2,
                AuthorId = 2,
                Author = user2,
                Tasks = Enumerable.Range(4, 6).Select(id => new ProjectTask() { Id = id, Name = "Task1" + id, PerformerId = 2, Performer = user2 })

            };
            Project proj3 = new()
            {
                Id = 3,
                AuthorId = 3,
                Author = user3,
                Tasks = Enumerable.Range(7, 9).Select(id => new ProjectTask() { Id = id, Name = "Task11" + id, PerformerId = 3, Performer = user3 })

            };

            connector.Data = new List<Project>() { proj1, proj2, proj3 };
            var actual = connector.GetUserTasks();
            Assert.NotNull(actual);
            Assert.Equal("Alice", actual.First().User.FirstName);
            Assert.True(IsDescendingSequence(actual.First().Tasks));
            actual.RemoveAt(0);
            Assert.Equal("Luccia", actual.First().User.FirstName);
            Assert.True(IsDescendingSequence(actual.First().Tasks));
            actual.RemoveAt(0);
            Assert.Equal("Vasya", actual.First().User.FirstName);
            Assert.True(IsDescendingSequence(actual.First().Tasks));
        }

        public bool IsDescendingSequence(List<ProjectTask> tasks)
        {

            for (int i = 0; i < tasks.Count - 1; i++)
            {
                if (string.Compare(tasks[i + 1].Name, tasks[i].Name) < 0)
                {
                    return false;
                }
            }
            return true;
        }
        [Fact]
        public void GetUserInfo_WhenUserHas2FinishedTasksAnd1Unfinished_Then2FinishedTasksAnd1Unfinished()
        {
            User user = new()
            {
                Id = 1,
                FirstName = "Simon",
                TeamId = 1,
                RegisteredAt = new System.DateTime(2010, 12, 2)
            };
            ProjectTask task1 = new ProjectTask
            {
                Id = 1,
                Performer = user,
                PerformerId = user.Id,
                CreatedAt = new System.DateTime(2020, 10, 1),
                FinishedAt = new System.DateTime(2021, 5, 1)
            };
            ProjectTask task2 = new ProjectTask
            {
                Id = 1,
                Performer = user,
                PerformerId = user.Id,
                CreatedAt = new System.DateTime(2021, 2, 1),
                FinishedAt = new System.DateTime(2021, 5, 1)
            };
            ProjectTask task3 = new ProjectTask
            {
                Id = 1,
                Performer = user,
                PerformerId = user.Id,
                CreatedAt = new System.DateTime(2021, 2, 1),
                FinishedAt = null
            };
            Project project = new()
            {
                Author = user,
                AuthorId = user.Id,
                Id = 11,
                Tasks = new List<ProjectTask>
                {
                    task1, task2, task3
                }
            };
            Connector connector = new();
            connector.Data = new List<Project>() { project };
            var actual = connector.GetUserInfo(1);
            Assert.Equal(3, actual.CountOfTasks);
            Assert.Equal(1, actual.AmountOfUnfinishedTasks);
            Assert.Equal(11, actual.LastProject.Id);
            Assert.Equal(1,actual.TheLongestTask.Id);
        }
        [Fact]
        public void GetProjectInfo_When_Then()
        {
            User user1 = new()
            {
                Id = 1,
                FirstName = "Simon",
                TeamId = 1
            };
            User user2 = new()
            {
                Id = 2,
                FirstName = "Alice",
                TeamId = 1
            };
            User user3 = new()
            {
                Id = 3,
                FirstName = "Bob",
                TeamId = 1
            };
            ProjectTask task1 = new ProjectTask
            {
                Id = 1,
                Performer = user1,
                PerformerId = user1.Id,
                CreatedAt = new System.DateTime(2020, 10, 1),
                FinishedAt = new System.DateTime(2021, 5, 1),
                ProjectId = 11,
                Description = "20symbolswillbwonthistaskdescription"
            };
            ProjectTask task2 = new ProjectTask
            {
                Id = 2,
                Performer = user2,
                PerformerId = user2.Id,
                CreatedAt = new System.DateTime(2021, 2, 1),
                FinishedAt = new System.DateTime(2021, 5, 1),
                ProjectId = 11,
                Description = "20symbolswillbwonthistaskdescription"
            };
            ProjectTask task3 = new ProjectTask
            {
                Id = 3,
                Performer = user3,
                PerformerId = user3.Id,
                CreatedAt = new System.DateTime(2021, 2, 1),
                FinishedAt = null,
                ProjectId = 11,
                Description = "20symbolswillbwonthistaskdescription"
            };
            ProjectTask task4 = new ProjectTask
            {
                Id = 4,
                Performer = user1,
                PerformerId = user1.Id,
                CreatedAt = new System.DateTime(2020, 10, 1),
                FinishedAt = new System.DateTime(2021, 5, 1),
                ProjectId = 12,
                Description = "20symbolswillbwonthistaskdescription"
            };
            ProjectTask task5 = new ProjectTask
            {
                Id = 5,
                Performer = user2,
                PerformerId = user2.Id,
                CreatedAt = new System.DateTime(2021, 2, 1),
                FinishedAt = new System.DateTime(2021, 5, 1),
                ProjectId = 12,
                Description = "20symbolswillbwonthistaskdescription"
            };
            ProjectTask task6 = new ProjectTask
            {
                Id = 6,
                Performer = user3,
                PerformerId = user3.Id,
                CreatedAt = new System.DateTime(2021, 2, 1),
                FinishedAt = null,
                ProjectId = 12,
                Description = "20symbolswillbwonthistaskdescription"
            };
            Team team = new()
            {
                Id = 1,
                Name = "CoolTeam"
            };
            Project project = new()
            {
                Team = team,
                TeamId = team.Id,
                Author = user1,
                AuthorId = user1.Id,
                Id = 11,
                Tasks = new List<ProjectTask>
                {
                    task1, task2, task3
                },
                Description = "Cool project with cool team and 3 tasks"
            };
            Project project2 = new()
            {
                Team = team,
                TeamId = team.Id,
                Author = user2,
                AuthorId = user2.Id,
                Id = 12,
                Tasks = new List<ProjectTask>
                {
                    task4, task5
                },
                Description = "Cool project with cool team and 2 tasks"
            };
            Connector connector = new Connector();
            connector.Data = new List<Project>() { project, project2 };
            var actual = connector.GetProjects().ToList();
            Assert.Equal(2,actual.Count);
        }

    }
}

